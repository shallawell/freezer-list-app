## Synopsis
A MEAN-APP to provide:
1. a web interface for a todo list 
2. Modified to be "whats in my freezer" app

## install nodejs, node package manager, mongoDB, download this git, then install this app
```
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.2.list
apt-get update
mkdir /data && mkdir /data/db
apt-get install -y nodejs npm 
apt-get install -y --allow-unauthenticated mongodb-org
git clone https://shallawell@gitlab.com/shallawell/freezer-list-app.git
mongod & 
cd freezer-list-app/ 
npm install nodemon 
npm install
ln -s "$(which nodejs)" /usr/bin/node
nodemon app.js
```

## Motivation
To further my personal software development skills and usage of tech such as MongoDB, Nodejs, Express framework, Front End APP development

## TO DO
0. add two columns (fluid containers)
1. update module versions where errors are reported in Docker build (probably means re-factor code) 
2. add token via passport.js for google (and remove cookie based auth) 
3. add more unit tests for each case (mocha, chai)
4. add real favicon.ico file to repo (currently empty placeholders)

## Author
@shallawell

## License
MIT

## Acknowledgements
(http://dreamerslab.com/blog/en/write-a-todo-list-with-express-and-mongodb/) <br>
(https://github.com/dreamerslab/express-todo-example/)